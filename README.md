[![pipeline status](https://gitlab.com/sophiabrandt/flask-react-auth/badges/master/pipeline.svg)](https://gitlab.com/sophiabrandt/flask-react-auth/commits/master)

<h1 align="center">Welcome to flask-react-aws 👋</h1>
<p>
  <a href="https://testdriven.io/courses/aws-flask-react/" target="_blank">
  </a>
</p>

> Deploying a Flask and React Microservice to AWS ECS

### 🏠 [Homepage](https://testdriven.io/courses/aws-flask-react/)

Learning repository. Please refer to **[Deploying a Flask and React Microservice to AWS ECS](https://testdriven.io/courses/aws-flask-react/)** for further information.

### ✨ [Demo](https://testdriven.io/courses/aws-flask-react/)

## LICENSE

The original project is © Michael Herman.

This repository is a mirror for learning purposes.

---

_This README was generated with ❤️ by [readme-md-generator](https://github.com/kefranabg/readme-md-generator)_
